import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter);
const Slot = () => import("../pages/Slot")
const Props = () => import("../pages/Props")
const router = new VueRouter({
  mode: "history",
  routes: [
    { path: '/slot', component: Slot },
    { path: '/props', component: Props },
  ],
});

export default router;