// 1.引入Vue
import Vue from "vue";
// 2.引入Vuex
import Vuex from "vuex"
//3.Vue使用这个插件
Vue.use(Vuex)
import axios from "axios";
// 创建一个state对象
const state = {
  count: 0, price: 998,
  movieList: []
}
// 创建一个mutation对象
const mutations = {
  add(state) {
    state.count++
  },
  sub(state) {
    state.count--
  },
  addFixed(state, { a }) {
    state.count += a
  },
  addMovieList(state, movieList) {
    state.movieList = movieList
  }
}
// 创建一个action
const actions = {
  // 接受commit
  delayed({ commit }, payload) {
    setTimeout(() => {
      commit("addFixed", payload)
    }, 2000);
  },
  // 发送axios请求
  async getMovieDataAxios({ commit }) {
    const res = await axios.get("https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a")
    commit("addMovieList", res.data.data.list)
  }
}
// 创建getters 类似计算属性
const getters = {
  myMovieList(state) {
    return state.movieList.filter(item => item.albumName.length > 4)
  },
  yourMovieList(state) {
    return ({ star }) => {
      return state.movieList.filter(item => item.albumName.length < star)
    }
  }
}
// 4.创建一个store 并暴漏出去
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})