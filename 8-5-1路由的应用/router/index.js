// 1.引入vue
import Vue from "vue";
// 2.引入Vue-router
import VueRouter from "vue-router";
//vue使用插件
Vue.use(VueRouter)
// 引入组件 路由懒加载的原理就是组件注册的时候使用异步组件
const Header = () => import("../page/Header.vue")
const Tab = () => import("../page/Tab.vue")
const Home = () => import("../page/Home.vue")
// 暴漏
export default new VueRouter({
  // 模式改为history 默认是hash
  model: history,
  routes: [
    {
      path: '/header', component: Header, name: "header"
    },
    {
      path: '/home', component: Home, name: "home",
      children: [
        {
          path: 'tab/:name/:id?',
          component: Tab,
          name: 'tab',
          meta: {
            now: 'tab'
          },
          props: (route) => ({
            ...route.params,
            ...route.query,
            ...route.meta
          })
        }
      ]
    },
    { path: '', redirect: '/home' }
  ]
})