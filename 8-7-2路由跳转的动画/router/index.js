// 引入Vue和路由
import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter)
import Body from '../pages/Body.vue'
import Header from '../pages/Header.vue'
export default new VueRouter({
  routes: [
    { path: '/body', component: Body, name: "body" },
    { path: '/header', component: Header, name: 'header' },
  ]
})