// 1.引入Vue
import Vue from "vue";
// 2.引入vuex
import Vuex from "vuex"
//3.Vue使用Vuex插件
Vue.use(Vuex)
import axios from "axios"
// 创建state
const state = {
  count: 0,
  movieList: []
}
// 创建mutations 是store改变状态的唯一方法
const mutations = {
  // 这里可以直接获得state 直接对他精选操作
  add(state) {
    state.count++
  },
  sub(state) {
    state.count--
  },
  addArb(state, payload) {
    state.count += payload.a
  },
  // 增加电影数量的函数
  addMovieList(state, movieList) {
    state.movieList = movieList
  }
}
// 创建Actions 类似mutations 但是这个主要来对异步的处理
const actions = {
  // 延时增加 第一个参数接受一个对象 第二个参数接受传来的值
  addWait({ commit }, payload) {
    setTimeout(() => {
      // 第一个参数是需要调用的函数 第二个参数是需要传的值
      commit("addArb", payload)
    }, 2000);
  },
  async getData({ commit }) {
    // 发送请求
    const res = await axios.get("https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a")
    // 修改数据
    commit("addMovieList", res.data.data.list)
  }
}
// 创建一个getters 类似计算属性
const getters = {
  myMovieList(state) {
    return state.movieList.filter(item => item.albumName.length > 4)
  },
  limitMovieList(state) {
    return ({ start, end }) => {
      return state.movieList.filter(item => item.albumName.length > start && item.albumName.length < end)
    }
  }
}
// 4.创建一个store 并暴漏出去 里面是一个配置对象
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})