// 1.引入vue
import Vue from "vue";
// 2.引入VueRouter
import VueRouter from "vue-router";
// 3.vue使用插件
Vue.use(VueRouter)
// 4.引入组件
import Tab from '../page/Tab.vue'
import Home from '../page/Home.vue'
import Game from '../page/Game.vue'
import Music from '../page/Music.vue'

// 5.创建router并暴漏出去
export default new VueRouter({
  routes: [
    { path: '/tab', component: Tab },
    {
      path: '/home', component: Home, props: true,
      children: [
        {
          path: 'game', component: Game, name: "game", props: (route) => { return route.query }
        },
        {
          path: 'music/:name/:id?', component: Music, name: "music", props: true
        },
      ]
    },
    { path: '/', redirect: '/home' }
  ]
})