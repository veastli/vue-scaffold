// 1.引入Vue
import Vue from "vue";
// 2.引入Vuex
import Vuex from "vuex"
// 3.Vue其实就是插件 需要Vue使用这个插件
Vue.use(Vuex)
// 创建一个state对象
const state = {
  count: 0,
  price: 999
}
// 4.创建一个store(Vuex的实例),参数是一个配置对象,未来在配置对象中可以配置action,mutations,state,getter
// 暴漏出去供vue实例的配置项挂载
export default new Vuex.Store({
  state
})