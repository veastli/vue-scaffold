import Vue from 'vue'
import App from './App.vue'

// 引入vue仓库 store
import store from "./store/store"
new Vue({
  render: h => h(App),
  store,
  beforeCreate() {
    Vue.prototype.$bus = this
  }
}).$mount('#app')