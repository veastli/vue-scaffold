import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter);
const One = () => import("../pages/one.vue");
const Two = () => import("../pages/two.vue");
const router = new VueRouter({
  mode: "history",
  routes: [{
    path: '/one', component: One,
    beforeEnter(to, from, next) {
      // 只要跳转到该路由即可生效
      console.log("我是路由独享守卫");
      next()
    }
  },
  {
    path: '/two/:id', component: Two
  },
  ],
});
// 全局前置守卫
router.beforeEach((to, from, next) => {
  console.log("我是全局前置守卫");
  next()
})
// 
router.beforeResolve((to, from, next) => {
  console.log("我是全局解析守卫");
  next()
})
// 全局后置钩子next都没有 只能做一些收尾工作 因为他已经要离开了
router.afterEach((to, from,) => {
  console.log("我是全局后置钩子");
})
export default router;