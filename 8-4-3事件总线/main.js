import Vue from "vue";
import App from "./App.vue";

// //引入路由器对象
// import router from "./router";

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  // 在初始化之前 在Vue的原型对象上放一个$bus属性,这样组件可以在这上面绑定事件
  beforeCreate() {
    Vue.prototype.$bus = this
  }
}).$mount("#app");
