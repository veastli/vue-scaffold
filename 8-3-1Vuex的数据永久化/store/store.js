// 1.引入Vue
import Vue from "vue";
// 2.引入Vuex
import Vuex from "vuex"
// 引入vuex-persistedstate帮我自动本地存储修改获取
import createPersistedState from "vuex-persistedstate"
// 3.Vue使用Vuex插件
Vue.use(Vuex)
// 4.引入模块
// 5.创建store并暴漏出去 暴露出去供Vue实例的配置项挂载
import data from "../store/data"
export default new Vuex.Store({
  //Store方法的配置对象接收一个modules,专门来配置vuex模块化的 
  modules: {
    data
  },
  plugins: [createPersistedState()]
})