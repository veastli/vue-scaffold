// 类似于mutation 主要是用来处理异步函数的
import axios from "axios"
export default {
  async getMovieList({ commit }) {
    const res = await axios.get("https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a")
    commit("setMovieList", res.data.data.list)
  }
}