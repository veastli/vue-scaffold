import actions from "./actions"
import getters from "./getters"
import state from "./state"
import mutations from "./mutations"
export default {
  //打开命名空间
  namespaced: true,
  actions, getters, state, mutations
}