// 更改store中状态的唯一方法,必须是同步函数,因为工具中无法追踪数据的变化
//mutation中的函数的第一个形参就是当前state的值mutation中的函数第二个参数载荷(payload)都是接受commit的传参
export default {
  // 加的函数
  add(state) {
    state.count++
    // localStorage.setItem("Vuex-count", JSON.stringify(state))
  },
  // 减的函数
  sub(state) {
    state.count--
    // localStorage.setItem("Vuex-count", JSON.stringify(state))
  },
  // 加特定的值
  addArb(state, { a }) {
    state.count += a
    // localStorage.setItem("Vuex-count", JSON.stringify(state))
  },
  // 数据清零
  Clear(state) {
    state.count = 0
    // localStorage.setItem("Vuex-count", JSON.stringify(state))
  },
  setMovieList(state, movieList) {
    state.movieList = movieList
    // localStorage.setItem("Vuex-count", JSON.stringify(state))
  },
}