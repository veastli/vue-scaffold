// Getters类似计算属性
export default {
  myMovieList(state) {
    return state.movieList.filter(item => item.albumName.length > 2 && item.albumName.length < 7)
  },
  SearchList(state) {
    return ({ key }) => {
      return state.movieList.filter(item => item.albumName.length > key)
    }
  }
}