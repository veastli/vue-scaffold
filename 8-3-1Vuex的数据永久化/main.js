import Vue from 'vue'
import App from './App.vue'
// 引入store并挂载
import store from './store/store'
new Vue({
  render: h => h(App),
  store,
  beforeCreate() {
    Vue.prototype.$bus = this
  }
}).$mount('#app')