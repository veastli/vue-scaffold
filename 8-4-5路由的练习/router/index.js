// 1.引入vue
import Vue from "vue";
// 2.引入Vue-router
import VueRouter from "vue-router";
//vue使用插件
Vue.use(VueRouter)
// 引入组件
import Header from "../page/Header.vue"
import Tab from "../page/Tab.vue"
import Home from "../page/Home.vue"
export default new VueRouter({
  routes: [
    {
      path: '/header', component: Header, name: "header"
    },
    {
      path: '/home', component: Home, name: "home"
      ,
      children: [
        {
          path: 'tab/:name/:id?',
          component: Tab,
          name: 'tab',

        }
      ]
    },
    { path: '', redirect: '/home' }
  ]
})