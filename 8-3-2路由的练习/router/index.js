// 1.引入Vue
import Vue from "vue";
// 2.引入VueRouter
import VueRouter from "vue-router"
// 3.Vue使用插件
Vue.use(VueRouter)
// 4.引入路由组件
import Header from "../pages/Header"
import Tab from "../pages/Tab"
import Music from "../pages/Header/Music"
import News from "../pages/Header/News"
import Game from "../pages/Header/Games"
// 5.创建路由器对象
export default new VueRouter({
  // 5.1在实例化路由器对象的配置中,书写路由表配置
  routes: [
    {
      path: '/header', component: Header,
      children: [
        { path: 'news', component: News, name: "News" },
        //如果在路由的配置中书写了props配置项,并且值为true,则代表把当前路由表接受的params动态参数通过props的形式传递给了该组件
        { path: 'music', props: true, component: Music, name: "Music", },
        // 在某个路由规则中如果默认的子路由
        {
          path: 'game', component: Game, name: "Game",
          props(route) {
            return { ...route.query }
          }
        },
      ]
    },
    { path: '/tab', component: Tab },
    // 初始的时候直接跳到Header显示
    { path: '/', component: Header }
  ]
})