import Vue from "vue";
import App from "./App.vue";

//引入路由器对象
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  //在Vue的配置对象中注册router,一旦注册完成 在所有的组件实例上都会出现$router和$route两个对象供我们使用
  router,
}).$mount("#app");
