import Header from '@/Plug/components/Header'
export default {
  install(Vue) {
    // 全局组件
    Vue.component('Header', Header)
    // 全局指令
    Vue.directive('Globel', (ele) => {
      ele.style.fontSize = '50px'
    })
    // 全局筛选
    Vue.filter('glo', (value) => value + '$#$#$#')
  }
}