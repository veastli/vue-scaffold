import Vue from 'vue'
import App from './App.vue'
import Child from '@/components/Child'
import Plug from './Plug'
// 全局指令的创建 第一个参数是指令的名字 第二个参数是函数
Vue.directive('newStyle', (ele) => {
  ele.style.color = 'blue'
})
// 注册全局组件 第一个参数是要取的名字 第二个参数是要引入的组件
Vue.component('Child', Child)
// 注册全局筛选器
Vue.filter("mark", (value) => value + '!!!!!')

// 使用插件Vue.use()
Vue.use(Plug)

new Vue({
  render: h => h(App),
}).$mount('#app')