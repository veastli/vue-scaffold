import Vue from 'vue'
import App from './App.vue'
import Student from '../src/components/Student'
// 全局的属性的创建需要一个个的创建 局部是统一放在VueComponent构造函数中创建
// 创建一个全局的属性 放置在Vue的原型对象身上
Vue.prototype.attribute = '欢迎使用Vue'
Vue.config.productionTip = false
// 必须要在挂载前写
Vue.component("Student", Student)
//  创建一个全局的过滤器
Vue.filter('formatDate', (value) => value + "岁")
// 创建一个全局的指令
Vue.directive('YouStyle', (ele) => {
  ele.style.color = 'red'
})
// 切记:所有的全局配置都需要在全局组件创建绑定之前设置 不然样式出不来
new Vue({
  render: h => h(App),
}).$mount('#app')
// 全局组件创建 在文件注册

