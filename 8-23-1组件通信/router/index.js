import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter);
const Props = () => import("../pages/Props");
const Customize = () => import("../pages/Customize");
const Vmodel = () => import("../pages/Vmodel");
const Sync = () => import("../pages/Sync");
const Ref = () => import("../pages/Ref");
const Slot = () => import("../pages/Slot");
const router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/props", component: Props },
    { path: "/customize", component: Customize },
    { path: "/vmodel", component: Vmodel },
    { path: "/sync", component: Sync },
    { path: "/ref", component: Ref },
    { path: "/slot", component: Slot },
  ],
});

export default router;