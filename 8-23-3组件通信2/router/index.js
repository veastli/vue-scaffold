import Vue from "vue"
import VueRouter from "vue-router"
Vue.use(VueRouter);
const Props = () => import("../pages/Props")
const Vmodel = () => import("../pages/Vmodel")
const Sync = () => import("../pages/Sync")
const Slot = () => import("../pages/Slot")
const router = new VueRouter({
  mode: "history",
  routes: [
    { path: '/props', component: Props },
    { path: '/vmodel', component: Vmodel },
    { path: '/sync', component: Sync },
    { path: '/slot', component: Slot },
  ],
});

export default router;