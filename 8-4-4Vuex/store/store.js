// 1.引入Vue
import Vue from "vue";
// 引入Vuex
import Vuex from "vuex"
import axios from "axios"
// Vue使用插件
Vue.use(Vuex)
// 创建state  驱动应用的数据源
const state = {
  count: 0,
  movieList: []
}
// 更改state中状态的唯一方法
const mutations = {
  add(state) {
    state.count++
  },
  sub(state) {
    state.count--
  },
  get(state, movieList) {
    state.movieList = movieList
  }
}
// 类似mutations,用来处理异步函数
const actions = {
  // api请求
  async getMovieList({ commit }) {
    const res = await axios.get("https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a")
    // 拿到请求到的数据进行state操作
    commit("get", res.data.data.list)
  }
}
// getters当state中的数据要经过处理后再使用,可以使用getters加工 类似Vue的计算属性
const getters = {
}
// 创建一个store,参数是一个配置对象 未来可以在配置对象中对配置
export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})