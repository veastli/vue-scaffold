// 1.引入Vue
import Vue from "vue";
// 2.引入VueRouter
import VueRouter from "vue-router"
// vue使用插件
Vue.use(VueRouter)
// 引入组件
import Home from "../pages/Home.vue"
import Tab from "../pages/Tab.vue"
import Game from "../pages/Game.vue"
import Music from "../pages/Music.vue"
import News from "../pages/News.vue"
// 创建并暴漏router实例对象
// props: true配置路由中打开 可以直接从props中获取
export default new VueRouter({
  routes: [
    {
      path: "/home", component: Home,
      children: [
        { path: 'Game/:id/:name?', component: Game, name: "Game", props: true },
        { path: 'Music', component: Music, props(route) { return { ...route.query } } },
        { path: 'News', component: News },
        { path: "", component: Music, }
      ]
    },
    { path: "/Tab", component: Tab },
    // redirect重定向 后面直接写路由路径
    { path: '/', redirect: "/home" }
  ]
})